Note. `https://gitlab.com` and `http://gitlab.localhost` are qasi identical. `http://git.localhost` represents the reduced host.

### What does it do?

It redirects HTTP(S) git traffic away from a primary GitLab domain to somewhere else.

Requests coming in on gitlab.localhost that match the git pattern will be redirected to git.localhost.  
This can be another LB/entrypoint.  
Any request made to git.localhost that does not match the git pattern will be redirected to gitlab.localhost.

Combined this enables us to split the traffic flow of git traffic from non-git traffic.

Like this:

```
# git verbose clone http://gitlab.localhost/T4cC0re/sonicrainboom-player.git 2>&1 | egrep -i 'gitlab|localhost'
17:21:24.114434 git.c:444               trace: built-in: git clone http://gitlab.localhost/T4cC0re/sonicrainboom-player.git
17:21:24.116408 run-command.c:663       trace: run_command: git-remote-http origin http://gitlab.localhost/T4cC0re/sonicrainboom-player.git
17:21:24.121369 http.c:756              == Info: Couldn't find host gitlab.localhost in the .netrc file; using defaults
17:21:24.122144 http.c:756              == Info: Connected to gitlab.localhost (::1) port 80 (#0)
17:21:24.122209 http.c:715              => Send header: Host: gitlab.localhost
17:21:24.122647 http.c:715              <= Recv header: location: http://git.localhost/T4cC0re/sonicrainboom-player.git/info/refs?service=git-upload-pack
17:21:24.122663 http.c:756              == Info: Connection #0 to host gitlab.localhost left intact
17:21:24.122680 http.c:756              == Info: Issue another request to this URL: 'http://git.localhost/T4cC0re/sonicrainboom-player.git/info/refs?service=git-upload-pack'
17:21:24.122708 http.c:756              == Info: Couldn't find host git.localhost in the .netrc file; using defaults
17:21:24.123183 http.c:756              == Info: Connected to git.localhost (::1) port 80 (#1)
17:21:24.123233 http.c:715              => Send header: Host: git.localhost
17:21:24.375311 http.c:715              <= Recv header: set-cookie: __cfduid=da0a36b33f3e8d30f602eb4862c01aeb61602256884; expires=Sun, 08-Nov-20 15:21:24 GMT; path=/; domain=.gitlab.com; HttpOnly; SameSite=Lax; Secure
17:21:24.375371 http.c:715              <= Recv header: gitlab-lb: fe-06-lb-gprd
17:21:24.375378 http.c:715              <= Recv header: gitlab-sv: git-18-sv-gprd
17:21:24.375422 http.c:715              <= Recv header: gitlab-git-traffic: side-channel
17:21:24.375471 http.c:756              == Info: Connection #1 to host git.localhost left intact
warning: redirecting to http://git.localhost/T4cC0re/sonicrainboom-player.git/
17:21:24.376216 run-command.c:663       trace: run_command: git fetch-pack --stateless-rpc --stdin --lock-pack --thin --check-self-contained-and-connected --cloning --no-progress http://git.localhost/T4cC0re/sonicrainboom-player.git/
17:21:24.378135 git.c:444               trace: built-in: git fetch-pack --stateless-rpc --stdin --lock-pack --thin --check-self-contained-and-connected --cloning --no-progress http://git.localhost/T4cC0re/sonicrainboom-player.git/
17:21:24.378808 http.c:756              == Info: Couldn't find host git.localhost in the .netrc file; using defaults
17:21:24.378823 http.c:756              == Info: Found bundle for host git.localhost: 0x80ed91a1780 [serially]
17:21:24.378834 http.c:756              == Info: Re-using existing connection! (#1) with host git.localhost
17:21:24.378842 http.c:756              == Info: Connected to git.localhost (::1) port 80 (#1)
17:21:24.378927 http.c:715              => Send header: Host: git.localhost
17:21:24.652476 http.c:715              <= Recv header: set-cookie: __cfduid=da0a36b33f3e8d30f602eb4862c01aeb61602256884; expires=Sun, 08-Nov-20 15:21:24 GMT; path=/; domain=.gitlab.com; HttpOnly; SameSite=Lax; Secure
17:21:24.652496 http.c:715              <= Recv header: gitlab-lb: fe-10-lb-gprd
17:21:24.652502 http.c:715              <= Recv header: gitlab-sv: gke-cny-git
17:21:24.652539 http.c:715              <= Recv header: gitlab-git-traffic: side-channel
Binary file (standard input) matches
```

`git verbose` being
```
verbose=!GIT_TRACE=2 GIT_CURL_VERBOSE=2 GIT_TRACE_PERFORMANCE=2 GIT_TRACE_PACK_ACCESS=2 GIT_TRACE_PACKET=2 GIT_TRACE_PACKFILE=2 GIT_TRACE_SETUP=2 GIT_TRACE_SHALLOW=2 GIT_MERGE_VERBOSITY=5 GIT_SSH_COMMAND='ssh -vvv -o ControlMaster=no' git
```


### What about web traffic to `/namespace/project.git`?

This usually when called from a web client triggers a redirect to `namespace/project`.  
This is preserved, but there is an intermediate redirect to `http://git.localhost/namespace/project.git` (then talking to git rails nodes) which issues the redirect to the target.

Like this:

```
# curl -IXGET -sSL http://gitlab.localhost/T4cC0re/sonicrainboom-player.git | grep -i location
location: http://git.localhost/T4cC0re/sonicrainboom-player.git
location: https://gitlab.com/T4cC0re/sonicrainboom-player
```

### How can we mitigate an attack on the git host?

I would suggest making the redirects flaggable, and still treat `gitlab.com`/`gitlab.localhost` as the canonical URL. That way in case of a (D)DoS or similar, we can turn of the redirects, and thus restore traffic flow to the old way if we need to.
